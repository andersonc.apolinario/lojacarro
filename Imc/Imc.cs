﻿using System;
using System.Collections.Generic;
using System.Text;


namespace CalculadoraIMC
{
    class IMC
    {
        /*public double Peso { get; set; }
        public double Altura { get; set; }
       */
        public double CalcularIMC(Usuario usuario)//usuario é uma estancia que vai chegar pelo parametro usuario "public é um metodo e precisa de um return
        
        {
            double imc = usuario.Peso /
                 (usuario.Altura * usuario.Altura);

            return imc;
        }

        public string ClassificarIMC(double imc)
        {
            if (imc < 18.5)
            {
                return "Baixo Peso";
            }
            else
            {
                if (imc >= 18.5 && imc <= 24.9)
                {
                    return "Peso normal";
                }
                else
                {
                    if (imc >= 25 && imc <= 29.9)
                    {
                        return "Pré-obeso";
                    }
                    else
                    {
                        if (imc >= 30 && imc <= 34.9)
                        {
                            return "Obeso I";
                        }
                        else
                        {
                            if (imc >= 35 && imc <= 39.9)
                            {
                                return "Obeso II";
                            }
                            else
                            {
                                return "Obeso III";
                            }
                        }
                    }
                }
            }
        }
    }
}

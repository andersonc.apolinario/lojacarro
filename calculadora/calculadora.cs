﻿using System;
using System.Collections.Generic;
using System.Text;

namespace calculadora
{
    public class Calculadora // foi precisa ser decladaa como publica 
    {
        //prof
        public double numA { get; set; } //CLASSE NUM A
        public double numB { get; set; }//CLASSE NUM B
        public string operacao { get; set; } // CLASSE OPERACAO

        public double ExecutarCalculo()//METODO QUANDO É VOID SE REFERE A VAZIO
        {

            //observacao: if de uma linha só nao precisa de chaves  entao foi retirado todos as chaves 
            //if (this.operacao == "somar")
            
            //    return this.numA + this.numB;// this se refere a classe 

            // else if (this.operacao == "subtrair")
           
            //    return this.numA - this.numB;

            // else if (this.operacao == "multiplicacao")
            
            //    return this.numA * this.numB;

            // else if (this.operacao == "divisao")
           
            //    return this.numA / this.numB ;
             
            //    return 0;


            switch (this.operacao) // o switch case utilizamos quando se tem ideia de menus exatos, quando nao existe muitas possibilidades de opcao
            {

                //obervacao utilizamos o THIS para fazer referencia a UMA CLASSE NO CASO NUMA E NUMB
                case "somar":
                    return this.numA + this.numB;

                case "subtrair":
                    return this.numA - this.numB;

                case "multiplicacao":
                    return this.numA * this.numB;

                case "divisao":
                    return this.numA / this.numB;

                default:
                    return 0;



            }

        }

    

    }
}

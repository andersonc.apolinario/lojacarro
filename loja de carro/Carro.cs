﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LojaCarro
{
    class Carro
    {
        //declarando propriedade usando getter e setters, dar prop e depois duas vezes o tab aparece o comando toodo//

        public int Ano { get; set; }
        public int NumeroDonos { get; set; }
        public string Marca { get; set; }
        public string Modelo { get; set; }
        public int Km { get; set; }
        public double Preco { get; set; }
        public string Cor { get; set; }

        public double ValorParcela(int parcelas)//
        {
            if (parcelas >= 24)
                return 0;
            Console.WriteLine("calculando parcelas");
            double valorParcela =
                this.Preco / parcelas;
            return valorParcela;
        }

        public string TipoCarro()
            
        {
            Console.WriteLine("verificando o tipo de carro");

            int idadeCarro = int.Parse(DateTime.Now.Year.ToString()) - this.Ano;

            double kmPorAno = this.Km / idadeCarro;

            //verificando se o carro é o zero
            if (this.Km == 0)
            {
                return "zero";
            }
               

            else if (idadeCarro <= 3)
            {
                if (this.NumeroDonos == 1)
                {
                    if(kmPorAno <= 20000)
                    {
                        return "seminovo";
                    } else
                        {
                        return "usado";
                        }

                }
                else {
                   return "usado";
                }

            } else
            {
                return "usado";
            }
            //return "";
            

        }


    }
}
